// Jordan Courvoisier
// Petr Shuller
// CS 240 with Ryan Parsons
// Assignment 3


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

// MyClient suggests a game based on genre preference. We are able to achieve this by having genre be the word in text
//  assotiator, and the various game the associations. Make it trivial to suggest something.
public class MyClient {

    public final static String THESAURUS_FILE = "ThesaurusFiles/input.txt";

    public static void main(String[] args) throws IOException {
        File file = new File(THESAURUS_FILE);

        //Create new empty TextAssociator
        TextAssociator textAssociator = new TextAssociator();

        // Populate text associator with stuff
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String text = null;

        while ((text = reader.readLine()) != null) {
            String[] words = text.split(",");
            String currWord = words[0].trim();
            textAssociator.addNewWord(currWord);

            for (int i = 1; i < words.length; i++) {
                textAssociator.addAssociation(currWord, words[i].trim());
            }
        }

        Scanner scan= new Scanner(System.in);
        String inputString = "";
        Random rand = new Random();

        while (true) {
            System.out.println("Options: platform, stealth, text, mmorpg, sandbox, fantasy, sim, rts, moba, td, tbs, wargame, racing, roguelike, rhythm, mmo, casual, idle, logic, programming, board ");
            System.out.print("Which genre are you interested in? (enter \"exit\" to exit):");

            inputString = scan.nextLine();
            if (inputString.equals("exit")) {
                break;
            }

            String[] tokens  = inputString.split(" ");
            Set<String> words = textAssociator.getAssociations(tokens[0].toLowerCase());

            if (words != null) {
                String result = "Try playing: " + words.toArray()[rand.nextInt(words.size())];
                System.out.println(result.trim());
            } else {
                System.out.println("Please input a valid option");
            }

            System.out.println();
        }
        reader.close();
    }
}
