// Jordan Courvoisier
// Petr Shuller
// CS 240 with Ryan Parsons
// Assignment 3

import java.lang.IllegalStateException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

// A TextAssociator efficiently stores associations between a word and given associations
public class TextAssociator {

    // Internal storage for chains
	private WordInfoSeparateChain[] table;

	// Keep track of number of words added (not associations)
	private int size;

	// Small number of primes that we can resize table to
	private final static int[] primes = {127, 353, 661, 1301, 3001, 6067, 12037, 24019, 50021, 100003, 202799, 400067, 1000003};

	// Internally used to keep track of resizing
	private int resizeTimes;

	/* INNER CLASS
	 * Represents a separate chain in your implementation of your hashing
	 * A WordInfoSeparateChain is a list of WordInfo objects that have all
	 * been hashed to the same index of the TextAssociator
	 */
    private class WordInfoSeparateChain {

        // Internal storage of our word infos
		private List<WordInfo> chain;
		
		/* Creates an empty WordInfoSeparateChain without any WordInfo
		 */
		public WordInfoSeparateChain() {
			this.chain = new ArrayList<WordInfo>();
		}
		
		/* Adds a WordInfo object to the SeparateChain
		 * Returns true if the WordInfo was successfully added, false otherwise
		 */
		public boolean add(WordInfo wi) {
			if (chain.contains(wi)) {
				return false;
			}

			chain.add(wi);
			return true;
		}
		
		/* Removes the given WordInfo object from the separate chain
		 * Returns true if the WordInfo was successfully removed, false otherwise
		 */
		public boolean remove(WordInfo wi) {
            if (!chain.contains(wi)) {
                return false;
            }

            chain.remove(wi);
            return true;
		}
		
		// Returns the size of this separate chain
		public int size() {
			return chain.size();
		}
		
		// Returns the String representation of this separate chain
		public String toString() {
			return chain.toString();
		}
		
		// Returns the list of WordInfo objects in this chain
		public List<WordInfo> getElements() {
			return chain;
		}
	}
	
	
	// Default constrcutor.
    // Creates a TextAssociator without any associations
	public TextAssociator() {
		table = new WordInfoSeparateChain[primes[0]];
	    size = 0;
        resizeTimes = 0;
	}

	// Pre: Word to be added
    // Post: Adds word if it is not yet in table
    //       Return true if successful, false if word already exists
	public boolean addNewWord(String word) {
	    if (wordExists(word)) {
	    	return false;
		} else if (table[index(word)] == null) {
		    WordInfoSeparateChain chain =  new WordInfoSeparateChain();

		    table[index(word)] = chain;
        }

        table[index(word)].add(new WordInfo(word));
        size++;

        if (size/table.length > 2) {
	        resize();
        }

        return true;
	}

	// Pre: Word that already exists, and association to associate with that word
    // Post: Adds an association between the given words.
    //      Returns true if successful, false if word is not in table, or if
    //      the association between the two words already exists
	public boolean addAssociation(String word, String association) {
		if (!wordExists(word)) {
		    return false;
        }

        WordInfo wordInfo = getWordsObject(word);

        for (String wordAssociation : wordInfo.getAssociations()) {
            if (wordAssociation.equals(association)) {
                return false;
            }
        }

        wordInfo.addAssociation(association);

        return true;
	}

	// Pre: Word to be removed
    // Post: Removes word and it's associations.
    //       Returns true if successful, false if word is not in table
	public boolean remove(String word) {
		if (!wordExists(word)) {
            return false;
        }

        WordInfo wordInfo = getWordsObject(word);
        table[index(word)].remove(wordInfo);
        size--;

        return true;
	}

	// Pre: Word whose associations are needed
    // Post: Returns set off all associations with the word.
    //       Returns null if word does not exist
	public Set<String> getAssociations(String word) {
		if (!wordExists(word)) {
            return null;
        }

        WordInfo wordInfo = getWordsObject(word);
		return wordInfo.getAssociations();
	}

	// Post: Prints the TextAssociator in a formatted manner to System.out
	public void prettyPrint() {
		System.out.println("Current number of elements : " + size);
		System.out.println("Current table size: " + table.length);
		
		//Walk through every possible index in the table
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null) {
				WordInfoSeparateChain bucket = table[i];
				
				//For each separate chain, grab each individual WordInfo
				for (WordInfo curr : bucket.getElements()) {
					System.out.println("\tin table index, " + i + ": " + curr);
				}
			}
		}
		System.out.println();
	}

    // Post: Updates to a next prime table size, and rehashes all words into new table
	private void resize() {
	    resizeTimes++;
	    size = 0; // We are effectively populating table again, so start at size zero

        WordInfoSeparateChain[] temp = table;
        table = new WordInfoSeparateChain[primes[resizeTimes]];

        for (WordInfoSeparateChain chain : temp) {
            if (chain != null) {
                for (WordInfo wordInfo : chain.getElements()) {

                    if (table[index(wordInfo.getWord())] == null) {
                        table[index(wordInfo.getWord())] = new WordInfoSeparateChain();
                    }

                    table[index(wordInfo.getWord())].add(wordInfo);
                }
            }
        }
    }

    // Pre: Word to be tested
    // Post: Return true if word is in table
    public boolean wordExists(String word) {
		if (table[index(word)] != null ) {
			List<WordInfo> chain = table[index(word)].getElements();

			for (WordInfo wordInfo : chain) {
				if (wordInfo.getWord().hashCode() == word.hashCode()) {
					return true;
				}
			}
		}

		return false;
	}

    // Pre: Word to get object. Throws IllegalStateException if word is not in table
    // Post: Returns object that has given word
	private WordInfo getWordsObject (String word) {
	    if (!wordExists(word)) {
	        throw new IllegalStateException("Can't get word if it doesn't exist");
        }

        List<WordInfo> chain = table[index(word)].getElements();

        WordInfo ourObject = null;
        
        for (WordInfo wordInfo : chain) {
            if (wordInfo.getWord().hashCode() == word.hashCode()) {
                ourObject = wordInfo;
                break;
            }
        }

        return ourObject;
    }

    // Pre: A word
    // Post: Returns the Hashed table position for a parameter given String
    private int index(String word) {
        return Math.abs(word.hashCode()  * 31 % table.length);
    }
}
