// Jordan Courvoisier
// Petr Shuller
// CS 240 with Ryan Parsons
// Assignment 3

import org.junit.Assert;
import org.junit.Test;

public class Tests {
    @Test
    public void testTest() {
        Assert.assertTrue(true);
    }

    @Test
    public void integrationTest() {
        TextAssociator associator = new TextAssociator();

        // Make sure we can add words, and detect that we have them
        associator.addNewWord("test");
        Assert.assertTrue(associator.wordExists("test"));

        // Make sure can't add same word twice
        Assert.assertFalse(associator.addNewWord("test"));


        associator.addAssociation("test", "hello world");
        associator.addAssociation("test", "anotha one");
        Assert.assertEquals("[anotha one, hello world]", associator.getAssociations("test").toString());

        associator.remove("test");
        Assert.assertFalse(associator.wordExists("test"));
        Assert.assertFalse(associator.remove("test"));
    }
}
