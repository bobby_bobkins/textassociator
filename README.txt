// Jordan Courvoisier
// Petr Shuller
// CS 240 with Ryan Parsons
// Assignment 2

1. What is your favorite sentence you generated with the ThesaurusClient as described above in how to run the ThesaurusClient?
    salute nation you is high old time en route to pencil set form and require crack a joke at all costs ruly English structures

2. For each of the three Design Decisions mentioned above in Implementation Notes, please discuss possible options that you considered, what you ended up choosing, and why.

For reference, the design decisions were:
    1. What should the starting capacity of your TextAssociator be?
    
    A) We made our default size 127. This is a number that is large enough to support many client while not being too large to conserve memory. It is also a prime number
       which makes it more useful for hashing. Just a big enough number so that we can avoid an initial handful of resizings.
    
    2. At what load factor should you expand your internal capacity? Remember that you must recalculate the destination of each WordInfo object when you expand your array.
       
    A) We chose to resize at a lambda factor of 2. As discussed in class, this is a decent choice as on average each chain will have 2 objects, so lookups are still constant.
           
    3. What should the new size of your array be?
    
    A) We have an Array of increasingly larger prime numbers that we choose the new size from, these are roughly twice 
       the size as before. This is again sort of a standard way to do it.

3.  What hash function did you choose for your TextAssociator (i.e. did you did you use String’s hashcode method, did you use the WordInfo's hashCode method, 
    did you incorporate your table size?) Why was this hash function effective, are there alternative hash functions that you considered? Also, please evaluate the hashCode 
    inside of WordInfo, even if you did not use it. Do you think it is a good hashCode and why?
    
    A) Our hash function is from WordInfo: a given String's hashCode * 31 then mod table size to find index. Since table size is prime and 31 is prime this leads to few collisions and works well
       as a hash code. It seemed like a decent function as 31 is prime, so we did not worry about coming up with an alternative. It seemed like a decent function because it uses the java builtin string hash as a base. It is unnecessary to be multiplying by 31, but we went with it because that is what was in WordInfo.
 

4.  We chose to implement this TextAssociator with separate chaining, if you were instead going to use a different collision resolution scheme,
    what would you choose? How and where would your code change? Give several specific examples to illustrate your understanding.
    
    A) If we used a different collision resolution technique we would use Quadratic probing. In our "addNewWord" method we would need to put collision checks
       to ensure that we did not find a collision. Because we use separate chaining we currently have no such checks. If we did find a Collision while adding
       we would need to call a new method we wrote to probe until we found a correct spot and return that index. We would use a separate method that could
       also be used in our find word method. Since we could now have values that had been hashed multiple times our "wordExists" method would need to 
       check all available spots until there was no longer a possibility the word existed.